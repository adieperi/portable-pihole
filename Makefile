.PHONY: all

all: start

update:
	docker exec pihole-portable pihole updateGravity
	docker-compose pull

start:
	docker-compose up -d

clean: stop rm

stop:
	docker-compose down

rm:
	sudo rm -rf etc-pihole/
