# Portable Pi-Hole

This is my portable pi-hole for my laptop

## How to run
You must rename the .env-example file to .env and if you wish you can change the informations.

### Start pi-hole
```Shell
make
```

### Stop pi-hole
```Shell
make stop
```

### Clean pi-hole
```Shell
make clean
```

### Update pi-hole
```Shell
make update
```
